## Hewan Ternak

### Deskripsi

Pak Dengklek mempunyai 2 jenis hewan ternak, yaitu ayam sebanyak $A$ ekor dan bebek sebanyak $B$ ekor. Anda ditugaskan Pak Dengklek untuk menghitung jumlah hewan ternak Pak Dengklek.

### Masukan

<pre>
A  B
</pre>

### Keluaran

Sebuah baris berisi nilai jumlah hewan ternak Pak Dengklek.

### Contoh Masukan 1

```
1 2
```

### Contoh Keluaran 1

```
3
```

### Contoh Masukan 1

```
4 1
```

### Contoh Keluaran 1

```
5
```

### Batasan

0 <= A, B <= 1000
